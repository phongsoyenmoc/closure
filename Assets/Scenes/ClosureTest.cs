﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// source reference :https://yinyangit.wordpress.com/2015/10/02/c-tim-hieu-ve-closure/ 
/// </summary>
public class ClosureTest : MonoBehaviour
{
    private void Start()
    {
        //Test();
        //Test2();
        Test3();
    }

    /// <summary>
    /// output in ra 5 số 5 vì 'Debug.Log(i) accesss to modifier closure'
    /// giá trị i không phải là giá trị tại thời điểm khai báo
    ///
    /// Như vậy bạn có thể thấy các Action (hay delegate) sẽ sử dụng các biến chung với môi trường mà delegate đó được tạo.
    /// Các biến mà bạn đem vào sử dụng bên trong delegate sẽ không phải là một giá trị cố định, tại thời điểm delegate được tạo ra.
    ///
    ///
    /// Trở lại vòng lặp ví dụ sử dụng vòng lặp phía trên.
    /// Lý do các action đều in ra kết quả 5 là do biến i sau khi chạy hết vòng lặp thì sẽ có giá trị là 5.
    /// Sau đó danh sách action mới bắt đầu được thực thi và tất nhiên chúng đều cùng in ra giá trị của biến i tại thời điểm đó như bạn thấy.
    /// </summary>
    private static void Test()
    {
        var actions = new List<Action>();
        for (var i = 0; i < 5; i++)
        {
            actions.Add(() => Debug.Log(i)); //closure
        }

        actions.ForEach(action => action());
    }

    /// <summary>
    /// Trong trường hợp bạn muốn cố định giá trị của delegate thì chỉ cần tạo ra một biến tạm để chứa giá trị và sử dụng nó trong delegate.
    /// Mỗi delegate sẽ sử dụng một biến riêng biệt và không bị thay đổi giá trị như sau:
    /// </summary>
    private static void Test2()
    {
        var actions = new List<Action>();
        for (var i = 0; i < 5; i++)
        {
            var j = i;
            actions.Add(() => Debug.Log(j));
        }

        actions.ForEach(action => action());
    }


    /// <summary>
    /// Sử dụng foreach lại khiến cho delegate được tạo ra có vẻ khiến chúng ta không thấy được sự hiện diện của closure.
    /// Nguyên nhân tất nhiên không phải .NET sai mà là do cơ chế của foreach. Thử dùng một trình reflector để xem lại chương trình, ta sẽ thấy lý do của nó.
    /// Xem Test4
    /// </summary>
    private void Test3()
    {
        var numbers = new int[] {0, 1, 2, 3, 4};
        var actions = new List<Action>();

        foreach (var number in numbers)
        {
            actions.Add(() => Debug.Log(number));
        }

        actions.ForEach(action => action());
    }

    /// <summary>
    /// Như bạn thấy foreach có cách hoạt động tương tự như vòng for thông thường và bên trong nó sử dụng một biến number để lưu giá trị của phần tử hiện tại trong mảng.
    /// Đó là nguyên nhân tại sao ví dụ phía trên lại có kết quả như thế.
    /// </summary>
    public void Test4()
    {
        int[] numArray = new int[] {0, 1, 2, 3, 4};
        List<Action> list = new List<Action>();
        int[] numArray2 = numArray;
        for (int i = 0; i < numArray2.Length; i++)
        {
            int number = numArray2[i];
            list.Add(() => Debug.Log(number));
        }
    }

    /// <summary>
    /// Thay vì sử dụng một mảng int, nếu bạn sử dụng một List thì foreach sẽ thế nào?
    /// 
    /// var numbers = new int[] { 0, 1, 2, 3, 4 };
    /// 
    /// thay bằng
    /// 
    /// var numbers = new List { 0, 1, 2, 3, 4 };
    ///
    /// Sử dụng reflector, bạn sẽ vẫn thấy nó tạo ra một biến có phạm vi bên trong vòng lặp (number).
    /// </summary>
    private void Test5()
    {
        List<int> list = new List<int> {0, 1, 2, 3, 4};
        List<Action> list2 = new List<Action>();
        using (List<int>.Enumerator enumerator = list.GetEnumerator())
        {
            while (enumerator.MoveNext())
            {
                int number = enumerator.Current;
                list2.Add(() => Debug.Log(number));
            }
        }
    }
}